# Generando números aleatorios de forma concurrente 

El **API de Java** nos proporciona una clase específica para generar números aleatorios en nuestras aplicaciones concurrentes. La clase es [`ThreadLocalRandom`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/ThreadLocalRandom.html) y es nueva desde la versión de Java 7. Funciona como si se tratara de una variable local del hilo. En cada hilo podremos generar un número aleatorio con un generador diferente, pero todos ellos gestionados por la misma clase, de forma transparente para el programador. Con este mecanismo, se obtiene un mejor rendimiento que utilizando un objeto compartido de la clase Random para generar números aleatorios en todos los hilos.

En este ejemplo, se mostrará el uso de la clase `ThreadLocalRandom` para generar números aleatorios en una aplicación concurrente.

1. Definimos una clase llamada `TaskLocalRandom` que implementa la interface `Runnable`.
2. En el constructor de la clase lo utilizaremos para inicializar el generador de números aleatorios para el hilo actual.

```java
...
/**
 * Constructor of the class. Initializes the randoom number generator
 * for this task
 */
public TaskLocalRandom() {
    ThreadLocalRandom.current();
}
...
```

3.- Implementamos el método `run()`. Obtenemos el nombre del hilo que está ejecutando la tarea actual y escribimos `10` números aleatorios en la salida estándar mediante el método `nextInt()`.

```java
...
/**
 * Main method of the class. Generate 10 random numbers and write them
 * in the console
 */
@Override
public void run() {
    String name=Thread.currentThread().getName();
    for (int i=0; i<10; i++){
        System.out.printf("%s: %d\n",name,ThreadLocalRandom.current().nextInt(10));
    }
}
...
```

4. Implementamos el método `main(.)` de la aplicación para probar la tarea creada anteriormente.
5. Creamos un array para almacenar tres objetos de la clase `Thread`.
6. Creamos tres tareas de la clase `TaskLocalRandom` y se las asociamos a los hilos para su ejecución.

```java
public static void main(String[] args) {
		
    /*
     * Create an array to store the threads 
     */
    Thread threads[]=new Thread[3];
		
    /*
     * Launch three tasks
     */
    for (int i=0; i<threads.length; i++) {
        TaskLocalRandom task=new TaskLocalRandom();
        threads[i]=new Thread(task);
        threads[i].start();
    }

}
```

7. Revisar la salida estándar para comprobar el resultado de la ejecución de las tareas.

## Información Adicional

La clase [`ThreadLocalRandom`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/ThreadLocalRandom.html) proporciona métodos para la generación de números de tipo `long`, `float`,  `double` y `Boolean`. También tenemos métodos que nos permiten proporcionar un número como parámetro para generar números aleatorios entre cero y ese número. Otros métodos nos permiten proporcionar dos números para generar números aleatorios en el intervalo que definen estos dos números. Revisar la documentación para una información más completa.
<!--stackedit_data:
eyJoaXN0b3J5IjpbMTc3MTgwODExXX0=
-->