# Usando variables atómicas

Las `variables atómicas` fueron introducidas en la versión de **Java 5** para proporcionar operaciones atómicas en variables individuales. Cuando utilizamos una variable normal, cada operación que implementamos en Java se transforma en diferentes instrucciones máquina cuando el programa se compila. Por ejemplo, cuando asignamos un valor en una variable, es sólo una instrucción en Java, pero cuando se compila el programa, esta instrucción se transforma en varias instrucciones en el lenguaje **JVM** (*Java Virtual Machine*). Este hecho puede provocar inconsistencia en los datos cuando múltiples hilos comparten una misma variable.

Para evitar este problema, Java nos proporciona las **variables atómicas**. Cuando un hilo utiliza una **variable atómica**, si otro hilo quiere realizar una operación sobre la misma variable, la implementación de la clase incluye los mecanismos para comprobar que la operación se ha completado en un solo paso. Básicamente, la operación obtiene el valor de la variable, cambia el valor en una variable local, y entonces intenta cambiar el valor anterior con el nuevo. Si el valor anterior sigue siendo el mismo, entonces lo cambia. Si no, el método realiza la operación desde el inicio otra vez. Esta operación se la conoce como **Compare and Set**.

Las **variables atómicas** no utilizan bloqueos o otros mecanismos de sincronización para proteger el acceso a sus valores. Todas sus operaciones están basadas en la operación **Compare and Set**. Está garantizado que diferentes hilos pueden trabajar con variables atómicas al mismo tiempo evitando la inconsistencia en los datos y su rendimiento es mejor que utilizando una variable normal protegida con mecanismos de sincronización.

En este ejemplo, se mostrará cómo se utilizan las **variables atómicas** implementando una cuenta bancaria y dos diferentes tareas, una que añade dinero a la cuenta y otra que retira dinero de la cuenta. Utilizaremos la clase [`AtomicLong`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/atomic/AtomicLong.html) para implementar el ejemplo.

1. Definimos una clase llamada `Account` para simular una cuenta bancaria.
2. El saldo de la cuenta será su variable de instancia y será de la clase `AtomicLong`.
3. Implementamos el constructor por defecto para inicializar la variable de instancia mediante el constructor de la clase `AtomicLong`.

```java
...
/**
 * Balance of the bank account
 */
private AtomicLong balance;
	
public Account(){
    balance=new AtomicLong();
}
...
```

4. Definimos los métodos de acceso para la variable de instancia. De esta forma podremos obtener el valor del saldo y asignar un valor al saldo. También necesitaremos un método para añadir un importe al saldo y para retirar un importe del saldo. Para los métodos que añaden al saldo y retiran del saldo se utilizan métodos de `AtomicLong` que implementen la operación **Compare and Set**. De esta forma será seguro añadir y retirar saldo de la cuenta.

```java
...
/**
 * Add an import to the balance of the account
 * @param amount import to add to the balance
 */
public void addAmount(long amount) {
    this.balance.getAndAdd(amount);
}
	
/**
 * Subtract an import to the balance of the account
 * @param amount import to subtract to the balance
 */
public void subtractAmount(long amount) {
    this.balance.getAndAdd(-amount);
}
...
```

5. Definimos una clase llamada `Company` que implementa la interface `Runnable`. Esta clase simulará pagos que realiza una compañía.
6. Como variable de instancia tenemos un objeto de la clase `Account` y el constructor establecerá el valor de la cuenta que utilizará la compañía para realizar los pagos establecidos.
7. Implementamos el método `run()` para simular `10` ingresos de `1000`€ en la cuenta.

```java
...
/**
 * Core method of the Runnable
 */
@Override
public void run() {
    for (int i=0; i<10; i++){
        account.addAmount(1000);
    }
}
...
```

8. Definimos una clase llamada `Bank` que implementa la interface `Runnable`. Esta clase simula la retirada de fondos de la cuenta.
9. La variable de instancia será un objeto de la clase `Account` y el constructor establecerá el valor de la cuenta que se utilizará para realizar las retiradas de fondos.
10. Implementamos el método `run()` para simulara `10` retiradas de `1000`€ de la cuenta.

```java
...
/**
 * Core method of the Runnable
 */
@Override
public void run() {
    for (int i=0; i<10; i++){
        account.subtractAmount(1000);
    }
}
...
```

11. Implementamos el método `main(.)` para comprobar el funcionamiento de las variables atómicas. Creamos un objeto de la clase `Account` con un saldo inicial de `1000`€.
12. Creamos dos tareas, una de la clase `Company` y otra de la clase `Bank` y le asignamos dos hilos para su ejecución. Se muestra por la salida estándar el estado inicial de la cuenta. Esperamos a la finalización de las tareas. Se presenta por la salida estándar el estado final de la cuenta.
13. Revisar el código de ejemplo del guión para comprobar el resultado.
14. Cambiar la clase `Account` y utilizar una variable normal para representar el saldo de la cuenta.
15. Comprobar el resultado y ver si se producen diferencias.

## Información Adicional

Como se ha mencionado en la introducción del ejemplo, tenemos más clases en Java para representar las operaciones sobre variables atómicas. Las clase son:

- [`AtomicBoolean`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/atomic/AtomicBoolean.html).
- [`AtomicInteger`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/atomic/AtomicInteger.html).
- [`AtomicReference`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/atomic/AtomicReference.html).
<!--stackedit_data:
eyJoaXN0b3J5IjpbMTY2MjQ3NzAwM119
-->