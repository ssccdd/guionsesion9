# Usando mapas navegables seguros

Una estructura de datos que tenemos a nuestra disposición en Java y que podemos utilizar en nuestros programas concurrentes es la interface [`ConcurrentNavigableMap`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/ConcurrentNavigableMap.html). Las clases que implementen esta interfaz almacenan los elementos en dos partes:

-   La **llave** que identifica de forma única un elemento.
-   El **valor** que definen al elemento que se guarda.
    
Cada parte tiene que ser implementada con diferentes clases. En el *API de Java* tenemos una clase que implementa esta interfaz, la clase es [`ConcurrentSkipListMap`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/ConcurrentSkipListMap.html) que implementa una lista sin bloqueo. Internamente, se usa una [**SkipList**](https://es.wikipedia.org/wiki/Skip_list) para almacenar los datos. Una **SkipList** es una estructura de datos basada en listas paralelas que nos permiten tener una eficiencia similar a los árboles binarios. Con ella, tenemos una estructura de datos con un mejor tiempo de acceso para insertar, buscar o borrar elementos que en una lista ordenada.

Cuando insertamos un elemento en el mapa, se utiliza una **llave** para ordenarlo, esto hace que todos los elementos estén ordenados. La clase también nos proporciona métodos para obtener un *submapa* del mapa original, además de poder obtener un elemento concreto.

En este ejemplo aprenderemos cómo utilizar la clase `ConcurrentSkipListMap` para implementar una lista de contactos.

1. Como primer paso, definimos una clase auxiliar para representar un contacto. La clase se llama `Contact`.
2. Como variables de instancia:
	- Un `String` para almacenar el nombre.
	- Un `String` para almacenar el teléfono.
3. Creamos un constructor para establecer los valores apropiados de las variables de instancia. Además creamos dos métodos para obtener los valores de cada una de las variables de instancia. Revisar el código de ejemplo del guión de prácticas.
4. La clase que representa la tarea la llamamos `Task` que implementa la interface `Runnable`.
5. Las variables de instancia serán:
	- Un objeto de la clase `ConcurrentSkipListMap` que tendrá como **llave** un objeto de la clase `String` y **valor** un objeto de la clase `Contact`. Será donde se almacenará la lista de contactos.
	- Un objeto de la clase `String` para almacenar el identificador de la tarea.
6. El constructor de la clase establecerá los valores apropiados para las variables de instancia.
7. Implementamos el método `run()` que tiene las acciones que realiza nuestra tarea. En este caso se crean `1000` contactos y se añaden al mapa con la **llave** generada por el número de teléfono y el identificador de la tarea.

```java
...
/**
 * Main method of the task. Generates 1000 contact objects and
 * store them in the navigable map
 */
@Override
public void run() {
    for (int i=0; i<1000; i++) {
        Contact contact=new Contact(id, String.valueOf(i+1000));
        map.put(id+contact.getPhone(), contact);
    }		
}
...
```

8. Implementamos el método `main(.)` de la aplicación para realizar una prueba. Para ello necesitamos un objeto de la clase `ConcurrentSkipListMap` que tendremos que pasar a las tareas.
9. Creamos un array para almacenar `25` objetos de la clase `Thread`.
10. Creamos `25` tareas identificadas desde la letra `A` hasta la letra `Z` y se las asociamos a los hilos para su ejecución.

```java
...
/*
 * Execute the 25 tasks
 */
for (char i='A'; i<'Z'; i++) {
    Task task=new Task(map, String.valueOf(i));
    threads[counter]=new Thread(task);
    threads[counter].start();
    counter++;
}
...
```

11. Esperamos hasta que finalicen las tareas.
12. Presentamos por la salida estándar diferentes estadísticas del mapa creado con la ejecución de las tareas. También se muestra la forma de extraer un *submapa* del mapa original.

```java
...
/*
 * Write a subset of the map 
 */
System.out.printf("Main: Submap from A1996 to B1002: \n");
ConcurrentNavigableMap<String, Contact> submap=map.subMap("A1996", "B1002");
do {
    element=submap.pollFirstEntry();
    if (element!=null) {
        contact=element.getValue();
        System.out.printf("%s: %s\n",contact.getName(),contact.getPhone());
    }
} while (element!=null);
...
```

## Información Adicional

Tenemos a nuestra disposición otros métodos para trabajar con el mapa:

- `headMap(K toKey)`: `K` es la clase que representa que representa la llave del mapa. El método devuelve un *submapa* con el primer elemento cuya **llave** es menor que la se ha pasado como parámetro.
- `tailMap(K toKey)`: `K` es la clase que representa la llave del mapa. El método devuelve un *submapa* con los últimos elementos cuya **llave** es mayor o igual que la se ha pasado como parámetro.
- `putIfAbsent(K key, V value)`: Este método inserta el **valor** especificado como parámetro con la **llave** aportada  si esa **llave** no se encuentra ya en el mapa.
- `pollLastEntry()`: Nos devuelve el último elemento del mapa (**llave**, **valor**) y lo elimina del mapa. Obtenemos un valor `null` si el mapa estuviera vacío.
- `replace(K key, V value)`: El método sustituye el **valor** asociado con la **llave** especificada como parámetro si la **llave** existe en el mapa.
<!--stackedit_data:
eyJoaXN0b3J5IjpbLTYwMjQ5NjE1N119
-->