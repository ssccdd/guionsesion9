# Usando arrays atómicos

Cuando desarrollamos una aplicación concurrente que tiene uno o más objetos compartidos por diferentes hilos, tenemos que proteger el acceso a sus atributos mediante mecanismos de sincronización que ya hemos presentado en guiones anteriores para evitar inconsistencia en los datos que puede provocar que las aplicaciones no funcionen correctamente.

Los mecanismos, presentados anteriormente, tienen los siguientes problemas:

- **Interbloqueo**: Esta situación se produce cuando un hilo está bloqueado a la espera que otro hilo lo libere. Y este segundo hilo también se encuentra bloqueado a la espera que el primer hilo también lo libere. Por tanto esto nunca sucederá. Es un error difícil de detectar y hace que nuestro programa nunca finalice.
- Si sólo un hilo tiene acceso al objeto compartido, tiene que ejecutar el código necesario para obtener y liberar el bloqueo.

Para proporcionar un mejor rendimiento a esta situación, la operación **Compare and Set** fue desarrollada. Esta operación implementa la modificación del valor de la variable en los siguientes tres pasos:

1. Se obtiene el valor de la variable, que es el valor antiguo de la variable.
2. Se cambia el valor de la variable en una variable temporal, que es el nuevo valor de la variable.
3. Se sustituye el valor antiguo con el nuevo valor, si el valor antiguo de la variable es igual que el valor actual de la variable. El valor antiguo de la variable puede ser diferente al actual si otro hilo cambió el valor de la variable. Si en este paso no se ha podido cambiar el valor de la variable, se volverá al primer punto.

Con este mecanismo, no serán necesarios mecanismos de sincronización, de esta forma se evitan los **interbloqueos** y se obtiene un mejor rendimiento.

Java implementa este mecanismo en las variables atómicas. Estas variables proporcionan el método `compareAndSet()` que es una implementación de la operación **Compare and Set** y otros métodos basados en él.

Java también introduce los arrays atómicos que proporcionan operaciones atómicas para array de números de tipo *integer* o *long*. En este ejemplo, se aprenderá cómo utilizar la clase [`AtomicIntegerArray`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/atomic/AtomicIntegerArray.html) para trabajar con arrays atómicos.

1. Definimos una clase llamada `Incrementer` que implementa la interface `Runnable`.
2. Tenemos como variable de instancia un objeto de la clase `AtomicIntegerArray` para simular el objeto compartido entre diferentes tareas.
3. El constructor de la clase asignará el valor apropiado a la variable de instancia para establecer el objeto compartido con otras tareas.
4. Implementamos el método `run()` para simular un incremento en todos los elementos del array.

```java
...
/**
 * Main method of the task. Increment all the elements of the
 * array
 */
@Override
public void run() {
    for (int i=0; i<vector.length(); i++){
        vector.getAndIncrement(i);
    }
}
...
```

5. Definimos una clase llamada `Decrementer` que implementa la interface `Runnable`.
6. Tenemos como variable de instancia un objeto de la clase `AtomicIntegerArray` para simular el objeto compartido entre diferentes tareas.
7. El constructor de la clase asignará el valor apropiado a la variable de instancia para establecer el objeto compartido con otras tareas.
8. Implementamos el método run() para simular un decremento en todos los elementos del array.

```java
...
/**
 * Main method of the class. It decrements all the elements of the 
 * array
 */
@Override
public void run() {
    for (int i=0; i<vector.length(); i++) {
        vector.getAndDecrement(i);
    }	
}
...
```

9. Implementamos el método `main(.)` para realizar una prueba de funcionamiento de los diferentes tipos de tareas que hemos definido anteriormente. Se comprueba que no hay problemas de inconsistencia en el array que comparten las diferentes tareas en sus operaciones de incremento y decremento.
10. Creamos el array que se compartirá entre las diferentes tareas con `1000` elementos.
11. Se crean dos array de la clase `Thread` con `1000` elementos para las tareas de incremento y decremento.
12. Se crean tareas de incremento y decremento y se les asignan los hilos para la realización de las tareas.

```java
...
/*
 * Create and execute 100 incrementer and 100 decrementer tasks
 */
Thread threadIncrementer[]=new Thread[THREADS];
Thread threadDecrementer[]=new Thread[THREADS];
for (int i=0; i<THREADS; i++) {
    threadIncrementer[i]=new Thread(incrementer);
    threadDecrementer[i]=new Thread(decrementer);
			
    threadIncrementer[i].start();
    threadDecrementer[i].start();
}
...
```

13. Se espera a la finalización de todas las tareas.
14. Se comprueba que no se ha producido inconsistencias en los datos, ya que se realizan los mismos incrementos y decrementos, y se presenta un mensaje de finalización de la aplicación.
15. Comprobar si se produce el mismo resultado con un array normal.

## Información Adicional

En este momento, Java proporciona otras dos clase para trabajar con arrays atómicos. Las clases `AtomicLongArray` y `AtomicReferenceArray` que proporcionan métodos similares que la clase [`AtomicIntegerArray`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/atomic/AtomicIntegerArray.html).

Otros métodos interesantes de la clase son:

- `get(int i)`: Devuelve el valor del array en la posición especificada por el parámetro.
- `set(int i, int newValue)`: Establece el valor del array en la posición especificada por el parámetro.

Revisad la documentación para comprobar todos los métodos a vuestra disposición y las posibilidades que tienen.
<!--stackedit_data:
eyJoaXN0b3J5IjpbLTE4NTU0NTcxODNdfQ==
-->