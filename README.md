[![logo](https://www.gnu.org/graphics/gplv3-127x51.png)](https://choosealicense.com/licenses/gpl-3.0/)
# GUIÓN DE PRÁCTICAS[^nota1]
## Sesión 9: Biblioteca de Estructuras Concurrentes (II)

En esta segunda parte del guión nos queda por repasar algunas estructuras seguras que tenemos a nuestra disposición en el [API de Java](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/package-frame.html). Las estructuras que nos quedan son:

-   Mapas navegables sin bloqueo, usando la clase [`ConcurrentSkipListMap`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/ConcurrentSkipListMap.html).
    
-   Números aleatorios, usando la clase [`ThreadLocalRandom`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/ThreadLocalRandom.html).
    
-   Variables atómicas, usando las clases [`AtomicLong`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/atomic/AtomicLong.html) y [`AtomicIntegerArray`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/atomic/AtomicIntegerArray.html).

Los ejemplos que muestran su utilización son:

1. [Usando mapas navegables seguros](https://gitlab.com/ssccdd/guionsesion9/-/blob/master/Ejemplo1.md)
2. [Generando números aleatorios de forma concurrente](https://gitlab.com/ssccdd/guionsesion9/-/blob/master/Ejemplo2.md)
3. [Usando variables atómicas](https://gitlab.com/ssccdd/guionsesion9/-/blob/master/Ejemplo3.md)
4. [Usando arrays atómicos](https://gitlab.com/ssccdd/guionsesion9/-/blob/master/Ejemplo4.md)

---
[^nota1]: El guión se extrae del libro *Java 7 Concurrency Cookbook* que se encuentra disponible para los alumnos de la [Universidad de Jaén](https://www.ujaen.es/) por medio de su servicio de [Biblioteca Digital](http://www.ujaen.debiblio.com/login?url=https://learning.oreilly.com/home/).
<!--stackedit_data:
eyJoaXN0b3J5IjpbMTg5ODg3MDUwMF19
-->